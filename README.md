# biblioHUB
C'est un projet personnel de gestion de livre et téléchargement d'ebook en plusieurs format avec une partie administration et authentification.
> Une app de gestion et de téléchargement de livre developpé en MERN-Stack.

**Hey!** :blush: il y a encore pas mal de bug à corriger et d'amélioration à faire que j'essayerai de corriger plus tard quand j'aurai le temps.

## :file_folder: Stack:
 1. Front-End:
     * React (Redux, Redux Thunk, Redux Form, Material-UI, Axios, Connected React Router, React Router, ...)
 2. Back-end:
     * NodeJs (Mongoose, Express, Passport, JsonWebToken, Multer, ...)
 3. Base de données:
     * MongoDB

## :cloud: Set up et Installation:
```sh
git clone https://tiavina30@bitbucket.org/tiavina30/biblio-hub.git
mongod                                    # run the database
cd ../server && npm install               # install the dependencies for the server
cd ../client && npm install               # install the dependencies for the client
cd ../server && npm start                 # run the server
cd ../client && npm start                 # run React
```


## :boy: Contact:
- Linkedin: https://www.linkedin.com/in/tiavina-michael-ralainirina/
- Skype: live:ralainirina
- GitHub: https://github.com/tiavina-mika